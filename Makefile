DEPLOY_SOURCE=dist
DEPLOY_TARGET=tilde.town:public_html/ritmas

DATE := $(shell date +%FT%T%Z)


.PHONY: default
default:
	@@echo "Usage: make task"


.PHONY: run
run:
	npm run dev


.PHONY: build
build:
	npm run build


.PHONY: serve-build
serve-build:
	npx http-server dist/ -p 3000

.PHONY: test
test:
	npm test

.PHONY: deploy
deploy:
	rsync -hlprt \
		--delete \
		--exclude .git/ \
		--exclude Makefile \
		--exclude node_modules/ \
		--exclude tmp/ \
		--exclude todo.txt \
		--exclude *.swp \
		"${DEPLOY_SOURCE}/" "${DEPLOY_TARGET}/"

.PHONY: publish
publish:
	git checkout pages
	cp -r dist/* ./
	cp -r dist/.domains .domains
	git commit -am "Published at ${DATE}."
	git push
	git checkout main
