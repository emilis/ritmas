import { defineConfig }     from 'vite'
import preact               from '@preact/preset-vite'


// https://vitejs.dev/config/
export default defineConfig({
    base:                   './',
    build: {
        rollupOptions: {
            output: {
                assetFileNames: 'assets/[name][extname]',
                entryFileNames: 'assets/[name].js',
            },
        },
        sourcemap:          true,
        target:             'es2022',
    },
    plugins: [
        preact(),
    ],
});
