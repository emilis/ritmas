import test                 from 'ava';

import {
    ANY_METRE,
    NO_SWING,
}                           from '../analysis/constants.js';
import findTiming           from '../analysis/find-timing.js';


const D1 =                  1;
const D2 =                  1/2;
const D4 =                  1/4;
const D8 =                  1/8;
const D16 =                 1/16;

const TESTS = [
    [   '4/4',  NO_SWING,   [ D4, D4, D4, D4 ]],
    [   '3/4',  NO_SWING,   [ D4, D4, D4 ]],
    [   '6/8',  NO_SWING,   [ D8, D8+D8, D8+D8, D8 ]],
    [   '9/8',  NO_SWING,   [ D8, D4, D8, D4, D8,D4 ]],
    [   '5/8',  NO_SWING,   [ D8+D8+D8, D8+D8 ]],
];


const repeat2 = durations => {
    const result =          [ 0, ...durations, 0, ...durations ];
    for( let i = 0; i < result.length; i++ ){
        result[i] = result[i] + (
            i ? result[ i - 1 ] : 0
        );
    }
    return result;
};

TESTS.forEach(([ metre, swing, durations ]) =>
    test( `${ metre } (${ swing }): ${ durations.join( ' ' )}`, t => {
        const taps =        repeat2( durations );
        const timing = findTiming({
            barCount:       1,
            metreFilter:    ANY_METRE,
            repeats:        2,
            taps,
        });

        //t.log( taps );
        t.is( timing.metre, metre );
        t.is( timing.swing, swing );
    })
);
