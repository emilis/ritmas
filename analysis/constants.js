export const ANY_METRE =            'any';
export const DEFAULT_METRE =        '4/4';
export const LESS_THAN_16_STEPS =   '<16';
export const NO_SWING =             '-';
