import { uniq }             from '../array/index.js';

import timings              from './timings.js';


export default uniq(
    timings.map( timing => timing.metre )
);
