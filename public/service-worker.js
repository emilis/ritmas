/// Constants ------------------------------------------------------------------

const CACHE_FILES = [
    './',
    './assets/index.css',
    './assets/index.js',
    './assets/noise.png',
    './header/logo.svg',
    './index.html',
    './ritmas.webmanifest',
    './service-worker.js',
];
const CACHE_KEY =           'ritmas';


/// Utils ----------------------------------------------------------------------

const addTimeToUrl = url =>
    `${ url }?t=${ Date.now() }`;


/// Communication --------------------------------------------------------------

const getClientWindows = () =>
    self.clients.matchAll({
        includeUncontrolled:    true,
        type:                   'window',
    });

const postToClients = async message => {
    for( const client of await getClientWindows() ){
        client.postMessage( message );
    }
};
const debug = ( ...args ) =>
    postToClients([ 'SW', ...args ]);


/// Cache management -----------------------------------------------------------

const fetchThroughCache = async request => {
    const cache =           await caches.open( CACHE_KEY );
    const response = await cache.match( request, {
        ignoreSearch:       true,
        ignoreVary:         true,
    });
    if( response ){
        debug( 'found', request.url );
        return response;
    } else {
        debug( 'miss', request.url );
        return fetch( addTimeToUrl( request.url ));
    }
};

const openCacheAddFiles = () =>
    caches.open( CACHE_KEY ).then( cache =>
        cache.addAll( CACHE_FILES.map( addTimeToUrl ))
    );

const reloadCache = async () => {

    for( const cacheKey of await caches.keys() ){
        const cache =           await caches.open( cacheKey );
        for( const request of await cache.keys() ){
            await cache.delete( request, {
                ignoreSearch:   true,
                ignoreVary:     true,
            });
            debug( 'cache.delete', cacheKey, request.url );
        }
        await caches.delete( cacheKey );
    }
    await openCacheAddFiles();
};


/// Main -----------------------------------------------------------------------

self.addEventListener( 'fetch', e => {
    debug( 'fetch', e.request.url );
    e.respondWith( fetchThroughCache( e.request ));
});

self.addEventListener( 'install', e => {
    debug( 'install' );
    e.waitUntil( openCacheAddFiles() );
});

self.addEventListener( 'message', async e => {
    debug( 'message', e.data );
    if( e.data === 'ritmas-update' ){
        debug( 'ritmas-update' );
        await reloadCache();
        await postToClients( 'ritmas-update-done' );
    }
});
