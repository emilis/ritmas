export default ( ...list ) =>
    list.filter( Boolean ).join( ' ' );
