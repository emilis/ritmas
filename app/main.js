import { h, render }        from '../eps/eps.modern.js';
import '../parameters/save-state.js';

import App                  from './App.js';


export default el =>
    render( h( App ), el );
